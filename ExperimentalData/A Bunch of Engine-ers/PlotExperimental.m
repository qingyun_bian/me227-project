figure(1)
subplot(4, 2, 1)
plot(t,r_radps);
title('Yaw-Rate Over Time')
xlabel('Time (s)');
ylabel('Yaw_rate (rad/s)');

subplot(4, 2, 2)
plot(t,Ux_mps);
title('Longitudinal Velocity Over Time')
xlabel('Time (s)');
ylabel('Longitudinal Velocity (m/s)');

subplot(4, 2, 3)
plot(t,Uy_mps);
title('Lateral Velocity Over Time')
xlabel('Time (s)');
ylabel('Lateral Velocity (m/s)');

subplot(4, 2, 4)
plot(t,deltaPsi);
title('Heading Error Over Time')
xlabel('Time (s)');
ylabel('Heading Error (rad)');

subplot(4, 2, 5)
plot(t,s_m);
title('Position on Path Over Time')
xlabel('Time (s)');
ylabel('Path Postion (m)');

subplot(4, 2, 6)
plot(t,e_m);
title('Lateral Error Over Time')
xlabel('Time (s)');
ylabel('Lateral Error (m)');

subplot(4, 2, 7)
plot(t,deltaCommand_rad);
hold on 
plot(t,deltaMeasured_rad);
title('Delta Command and Measured Over Time ')
xlabel('Time (s)');
ylabel('Delta (rad)');
legend('Delta Command', 'Delta Measured')

subplot(4, 2, 8)
plot(t, FxCommand_N)
title('Longitudinal Force Fx Over Time ')
xlabel('Time (s)');
ylabel('Longitudinal Force Fx (N)');
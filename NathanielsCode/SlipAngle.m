function [alpha_f, alpha_r] = SlipAngle(Uy,Ux,delta,r,a,b) %SlipAngle calculates the slip angle of the front tires
% Detailed explanation goes here
alpha_r = (Uy-b*r)/Ux;
alpha_f = ((Uy+a*r)/Ux)-delta;
end

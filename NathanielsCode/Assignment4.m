%% Problem One
clear all
close all

%Define Constants
Caf = 188000;       %N/rad
Car = 203000;       %N/rad
m = 1648;           %kg
Iz = 2235;          %kg*m^23
L = 2.468;          %m
%Weight distribution
W_f = 0.577;
W_r = 0.423;
a = W_r*L;          %m
b = W_f*L;          %m

%Initialize variables
x_la = 0;
psi = 0;
e = -1;                     %m
delta_deg = 3;              %deg
delta = deg2rad(delta_deg); %rad

%% Part 1a

%Solve for Kla
Kla_part_a = -Caf*delta/(e+x_la*psi);
Kla = Kla_part_a;

%% Part 1b
close all

%Create a vector of longitudinal speeds
Ux = (5:5:30);              %m/s

Poles = zeros(4,length(Ux));

for i = 1:length(Ux)
    %System matrix
    SYS = [0 1 0 0;...
        -Kla/m -(Caf+Car)/(m*Ux(i)) ((Caf+Car)/m)-(Kla*x_la/m) (-a*Caf+b*Car)/(m*Ux(i));...
        0 0 0 1;...
        -Kla*a/Iz (b*Car-a*Caf)/(Iz*Ux(i)) (a*Caf-b*Car)/Iz-Kla*a*x_la/Iz -(a^2*Caf+b^2*Car)/(Iz*Ux(i))];
    %Get the poles of the system form the eigen values
    Poles(:,i) = eig(SYS)
end
plot(Poles,'X');
title('Stability');
xlabel('Real');
ylabel('Imaginary');
legend('5m/s','10m/s','15m/s','20m/s','25m/s','30m/s');
grid on
plotfixer

%% Part 1c
close all

Ux = 15;                    %m/s
x_la = 10;                  %m
Kla = (1000:1000:10000);    %N/m

Poles = zeros(4,length(Kla));

for i = 1:length(Kla)
    %System matrix
    SYS = [0 1 0 0;...
        -Kla(i)/m -(Caf+Car)/(m*Ux) ((Caf+Car)/m)-(Kla(i)*x_la/m) (-a*Caf+b*Car)/(m*Ux);...
        0 0 0 1;...
        -Kla(i)*a/Iz (b*Car-a*Caf)/(Iz*Ux) (a*Caf-b*Car)/Iz-Kla(i)*a*x_la/Iz -(a^2*Caf+b^2*Car)/(Iz*Ux)];
    %Get the poles of the system form the eigen values
    Poles(:,i) = eig(SYS)
end
plot(Poles,'X');
title('Stability');
xlabel('Real');
ylabel('Imaginary');
legend('1000N/m','2000N/m','3000N/m','4000N/m','5000N/m','6000N/m',...
    '7000N/m','8000N/m','9000N/m','10000N/m');
grid on
plotfixer

%% Part 1d
close all
clear e

Ux = 15;                    %m/s
x_la = 15;                  %m
Kla = [1000 10000];         %N/m

e_0 = 1;                    %m
t = (0:0.01:10);            %s
U = zeros(length(t),1);       %no inputs
X0 = [e_0 0 0 0];

for i = 1:length(Kla)
    %System matrix
    A = [0 1 0 0;...
        -Kla(i)/m -(Caf+Car)/(m*Ux) ((Caf+Car)/m)-(Kla(i)*x_la/m) (-a*Caf+b*Car)/(m*Ux);...
        0 0 0 1;...
        -Kla(i)*a/Iz (b*Car-a*Caf)/(Iz*Ux) (a*Caf-b*Car)/Iz-Kla(i)*a*x_la/Iz -(a^2*Caf+b^2*Car)/(Iz*Ux)];
%     y_dot = SYS.*y;
    B = zeros(4,1);
    C = eye(4);
    D = zeros(4,1);
    SYS = ss(A,B,C,D)
    Result = lsim(SYS,U,t,X0);
    plot(t,Result(:,1));
    hold on
end

title('Lateral Error Over Time');
xlabel('Time (s)');
ylabel('Lateral Error (m)');
legend('K = 1000','K = 10000');

plotfixer

%% Part 1e

close all

Ux = 15;                    %m/s
x_la = (2:2:20);            %m
Kla = Kla_part_a;           %N/m

Poles = zeros(4,length(x_la));

for i = 1:length(x_la)
    %System matrix
    SYS = [0 1 0 0;...
        -Kla/m -(Caf+Car)/(m*Ux) ((Caf+Car)/m)-(Kla*x_la(i)/m) (-a*Caf+b*Car)/(m*Ux);...
        0 0 0 1;...
        -Kla*a/Iz (b*Car-a*Caf)/(Iz*Ux) (a*Caf-b*Car)/Iz-Kla*a*x_la(i)/Iz -(a^2*Caf+b^2*Car)/(Iz*Ux)];
    %Get the poles of the system form the eigen values
    Poles(:,i) = eig(SYS)
end
plot(Poles,'X');
title('Stability');
xlabel('Real');
ylabel('Imaginary');
legend('2m','4m','6m','8m','10m','12m',...
    '14m','16m','18m','20m');
grid on

%% Part 1f
close all
clear e

Ux = 15;                    %m/s
x_la = [2 20];              %m
Kla = Kla_part_a;           %N/m

e_0 = 1;                    %m
t = (0:0.01:10);            %s
U = zeros(length(t),1);     %no inputs
X0 = [e_0 0 0 0];           %Initial conditions

for i = 1:length(x_la)
    %System matrix
    A = [0 1 0 0;...
        -Kla/m -(Caf+Car)/(m*Ux) ((Caf+Car)/m)-(Kla*x_la(i)/m) (-a*Caf+b*Car)/(m*Ux);...
        0 0 0 1;...
        -Kla*a/Iz (b*Car-a*Caf)/(Iz*Ux) (a*Caf-b*Car)/Iz-Kla*a*x_la(i)/Iz -(a^2*Caf+b^2*Car)/(Iz*Ux)];
    B = zeros(4,1);
    C = eye(4);
    D = zeros(4,1);
    SYS = ss(A,B,C,D);
    Result = lsim(SYS,U,t,X0);
    plot(t,Result(:,1));
    hold on
end

title('Lateral Error Over Time');
xlabel('Time (s)');
ylabel('Lateral Error (m)');
legend('x_{la} = 2m',' x_{la}= 20m');

plotfixer

%% Part 2a
close all

W_f = 0.3;
W_r = 0.7;
a = W_r*L;          %m
b = W_f*L;          %m

K = (m/L)*((b/Caf)-(a/Car))
Vch = sqrt(L/(-K))

%% Part 2b
close all

Ux = 30;            %m/s
Kla = 3500;         %N/m
x_la = (0:2:30);    %m

Poles = zeros(4,length(x_la));

for i = 1:length(x_la)
    %System matrix
    SYS = [0 1 0 0;...
        -Kla/m -(Caf+Car)/(m*Ux) ((Caf+Car)/m)-(Kla*x_la(i)/m) (-a*Caf+b*Car)/(m*Ux);...
        0 0 0 1;...
        -Kla*a/Iz (b*Car-a*Caf)/(Iz*Ux) (a*Caf-b*Car)/Iz-Kla*a*x_la(i)/Iz -(a^2*Caf+b^2*Car)/(Iz*Ux)];
    %Get the poles of the system form the eigen values
    Poles(:,i) = eig(SYS)
end
plot(Poles,'X');
title('Stability');
xlabel('Real');
ylabel('Imaginary');
legend('0','2m','4m','6m','8m','10m','12m','14m','16m','18m','20m',...
    '22m','24m','26m','28m','30m');
grid on

plotfixer

%% Part 2c

close all
clear e

Ux = 30;                    %m/s
x_la = 25;                  %m
Kla = 3500;                 %N/m

e_0 = 1;                   %m
t = (0:0.01:10);            %s
U = zeros(length(t),1);     %no inputs
X0 = [e_0 0 0 0];           %Initial conditions

for i = 1:length(x_la)
    %System matrix
    A = [0 1 0 0;...
        -Kla/m -(Caf+Car)/(m*Ux) ((Caf+Car)/m)-(Kla*x_la(i)/m) (-a*Caf+b*Car)/(m*Ux);...
        0 0 0 1;...
        -Kla*a/Iz (b*Car-a*Caf)/(Iz*Ux) (a*Caf-b*Car)/Iz-Kla*a*x_la(i)/Iz -(a^2*Caf+b^2*Car)/(Iz*Ux)];
    B = zeros(4,1);
    C = eye(4);
    D = zeros(4,1);
    SYS = ss(A,B,C,D);
    Result = lsim(SYS,U,t,X0);
    
    figure(1)
    plot(t,Result(:,1));
    hold on
    figure(2)
    plot(t,Result(:,3));
end

figure(1)
title('Lateral Error Over Time');
xlabel('Time (s)');
ylabel('Lateral Error (m)');

figure(2)
title('Heading Error Over Time');
xlabel('Time (s)');
ylabel('Heading Error (rad)');

plotfixer

%% Part 3
close all

%Define Inertia constants
g = 9.81;                       %gravitational acceleration (m/s^2)

%Define the nonlinear model constants for Shelly
ConstNL.C_af      = 275000;               %Front wheel stiffness      (N/rad)
ConstNL.C_ar      = 265000;               %Rear wheel stiffness       (N/rad)
ConstNL.C_af_linear = 188000;             %Linear front wheel stiffness (N/rad)
ConstNL.C_ar_linear = 203000;             %Linear front wheel stiffness (N/rad)
ConstNL.m         = m;                    %Mass                       (kg)
ConstNL.Iz        = Iz;                   %Ineria                     (kg/m^2)
ConstNL.L         = L;                    %Wheelbase                  (m)
ConstNL.Wf        = 0.577*ConstNL.m*g;    %Front weight distribution  (%)
ConstNL.Wr        = 0.423*ConstNL.m*g;    %Rear weight distribution   (%)
ConstNL.a         = 0.423*ConstNL.L;      %Length from front to COM   (m)
ConstNL.b         = 0.577*ConstNL.L;      %Length from rear to COM    (m)
ConstNL.Front     = 1;
ConstNL.Rear      = 2;
ConstNL.Kla       = 3500;                 %Look ahead gain            (N/m)

ConstNL.uf      = 0.97;                 %Front wheel friciton   coefficient
ConstNL.ufs     = 0.97;                 %Front wheel friciton   coefficient
ConstNL.ur      = 1.03;                 %Rear wheel friciton    coefficient
ConstNL.urs     = 1.03;                 %Front wheel friciton   coefficient

%Define Kdrive
ax = 0.1*g;                             %m/s^2
Fxtot = ax*m;                           %g
Udiff = 1;                              %m/s
ConstNL.Kdr = Fxtot/Udiff;              %N/m

%Define the input for the nonlinear simulation
Input.Kappa = 0;
Input.xla   = 15;
Input.Uxdes  = 15;

%Define initial conditions in the input struct
Input.e     = 1;
Input.Ux    = 13;

% simulation time
t_final = 8;
Time.dT = 0.001;
t_s = 0:Time.dT:t_final;
Time.N = length(t_s);

% straight  path
s = [0     150];
k = [0     0];

% integrate s and k with given initial conditions [psi0, E0, N0] to get path
path = generate_path(s,k,[0;0;0]);

[r,Ux,Uy,psi,s,e] = NonLinearSysSim(ConstNL,Input,Time,path);

figure(1)
plot(t_s,r);
title('Yaw-Rate Over Time')
xlabel('Time (s)');
ylabel('Yaw_rate (rad/s)');

figure(2)
plot(t_s,Ux);
title('Longitudinal Velocity Over Time')
xlabel('Time (s)');
ylabel('Longitudinal Velocity (m/s)');

figure(3)
plot(t_s,Uy);
title('Lateral Velocity Over Time')
xlabel('Time (s)');
ylabel('Lateral Velocity (m/s)');

figure(4)
plot(t_s,psi);
title('Heading Error Over Time')
xlabel('Time (s)');
ylabel('Heading Error (rad)');

figure(5)
plot(t_s,s);
title('Position on Path Over Time')
xlabel('Time (s)');
ylabel('Path Postion (m)');

figure(6)
plot(t_s,e);
title('Lateral Error Over Time')
xlabel('Time (s)');
ylabel('Lateral Error (m)');

%Part b
close all

%Define initial conditions in the input struct
Input.e     = 1;
Input.Ux    = 15;

% constant radius  path
s = [0      150];
k = [1/40   1/40];

% % integrate s and k with given initial conditions [psi0, E0, N0] to get path
path = generate_path(s,k,[0;0;0]);

[r_c,Ux_c,Uy_c,psi_c,s_c,e_c] = NonLinearSysSim(ConstNL,Input,Time,path);

% "undulating" path
s_u = [0      20      40   150];
k_u = [1/40   -1/40   0    0];

% integrate s and k with given initial conditions [psi0, E0, N0] to get path
path_u = generate_path(s_u,k_u,[0;0;0]);

[r_u,Ux_u,Uy_u,psi_u,s_u,e_u] = NonLinearSysSim(ConstNL,Input,Time,path_u);

figure(1)
plot(t_s,psi_c);
hold on
plot(t_s,psi_u);
title('Heading Error Over Time');
xlabel('Time (s)');
ylabel('Error');
legend('Constant Path','Undulating Path');

figure(2)
plot(t_s,e_c);
hold on
plot(t_s,e_u);
title('Lateral Error Over Time');
xlabel('Time (s)');
ylabel('Error');
legend('Constant Path','Undulating Path');


plotfixer
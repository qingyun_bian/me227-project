function [Force_f,Force_r] = SlipForcesNL(Const,Input)
%SlipForcesNL Determines the nonlinear froces on the tires
%   Detailed explanation goes here

%Redefine Constants into simpler variables
C_af = Const.C_af;
C_ar = Const.C_ar;
AlphaF = Input.AlphaF;
AlphaR = Input.AlphaR;
AlphaSlipF = Input.AlphaSlipF;
AlphaSlipR = Input.AlphaSlipR;
uf = Const.uf;
ur = Const.ur;
ufs = Const.ufs;
urs = Const.urs;
Wf = Const.Wf;
Wr = Const.Wr;

%Check for the front peak slip angle
if abs(AlphaF) < AlphaSlipF
    %Calculate the force on the front tire
    Force_f = -C_af*tan(AlphaF) + ...
        ((C_af^2)/(3*uf*Wf))*(2-(ufs/uf))*abs(tan(AlphaF))*tan(AlphaF) - ...
        ((C_af^3)/(9*(uf^2)*(Wf^2)))*((tan(AlphaF))^3)*(1-(2*ufs/(3*uf)));
else
    %Calculate the force on the front tire
    Force_f = -ufs*Wf*sign(AlphaF);
end

%Check for the rear peak slip angle
if abs(AlphaR) < AlphaSlipR
    %Calculate the force on the rear tire
    Force_r = -C_ar*tan(AlphaR) + ...
        ((C_ar^2)/(3*ur*Wr))*(2-(urs/ur))*abs(tan(AlphaR))*tan(AlphaR) - ...
        ((C_ar^3)/(9*(ur^2)*(Wr^2)))*((tan(AlphaR))^3)*(1-(2*urs/(3*ur)));
else
    %Calculate the force on the rear tire
    Force_r = -urs*Wr*sign(AlphaR);
end
end


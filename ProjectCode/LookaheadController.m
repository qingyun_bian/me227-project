function [delta_rad,Fx_N] = LookaheadController(s_m,e_m,Ux,psi,path)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    
    g = 9.81;                       %gravitational acceleration (m/s^2)
    m = 1659;                       %kg      
    L = 2.468;                      %m
    
	C_af_linear = 188000;           %Linear front wheel stiffness (N/rad)
	C_ar_linear = 203000;           %Linear front wheel stiffness (N/rad)
  	a = 0.423 * L;                  %Length from front to COM   (m)
	b = 0.577 * L;                  %Length from rear to COM    (m)

    Kdr = 5100;
    Kff = 1500;
    Ux_des_m = path.Ux_des;
    Uxdot_des_m = path.Uxdot_des;
    
    frr = 0.0157;                % coefficient of rolling resistance
	Frr = frr * m * g;
    rho = 1.225;
    C_DA = 0.594;

    %Lookahead gain
    Kla = 12000;
    xla = 15; 
    
    %Limitations of force output to stay within acceleration (m*a_max)
    Fx_max = m*3;
    Fx_min = m*(-4);
    
    % look up K for path in parking lot
    Kappa = interp1(path.s_m, path.k_1pm, s_m);
    
    %Calculate delta
    K = (m/L)*((b/C_af_linear)-(a/C_ar_linear));
    psi_ss = Kappa*(((m*a*(Ux^2))/(L*C_ar_linear)) - b);
    delta_ff = (Kla*xla/C_af_linear)*psi_ss + Kappa*(L + K*(Ux^2));
    delta_rad = (-Kla*(e_m + xla*psi)/C_af_linear) + delta_ff;

    %Interpolate to get Ux_des and Uxdot_des wrt time step
    Ux_des = interp1(path.s_m, Ux_des_m, s_m);
    Uxdot_des = interp1(path.s_m, Uxdot_des_m, s_m);
    
    %Calculate the longitudinal forces on each wheel
    Fd = rho * Ux^2 * C_DA/2;
    Fx = Kdr*(Ux_des-Ux) + Kff*Uxdot_des + Frr + Fd ;%+ randn(1)*3400;
    if Fx > Fx_max
        Fx_N = Fx_max;
    elseif Fx < Fx_min
        Fx_N = Fx_min;
    else
        Fx_N = Fx;
    end
end


function [Force_f,Force_r] = SlipForcesNLCoupled(Const,Input,Fx)
%SlipForcesNL Determines the nonlinear froces on the tires
%   Detailed explanation goes here

%Redefine Constants into simpler variables
C_af = Const.C_af;
C_ar = Const.C_ar;
AlphaF = Input.AlphaF;
AlphaR = Input.AlphaR;
uf = Const.uf;
ur = Const.ur;
ufs = uf;
urs = ur;
Wf = Const.Wf;
Wr = Const.Wr;
Front = Const.Front;
Rear = Const.Rear;

%Coupled consts
zeta_f = sqrt((uf*Wf)^2-Fx(Front)^2)/(uf*Wf);
zeta_r = sqrt((ur*Wr)^2-Fx(Rear)^2)/(ur*Wr);
AlphaSlipF = atan(3*zeta_f*uf*Wf/C_af);
AlphaSlipR = atan(3*zeta_r*ur*Wr/C_ar);


%Check for the front peak slip angle
if abs(AlphaF) < AlphaSlipF
    %Calculate the force on the front tire
    Force_f = -C_af*tan(AlphaF) + ...
        ((C_af^2)/(3*zeta_f*uf*Wf))*abs(tan(AlphaF))*tan(AlphaF) - ...
        ((C_af^3)/(27*(zeta_f^2)*(uf^2)*(Wf^2)))*((tan(AlphaF))^3);
else
    %Calculate the force on the front tire
    Force_f = -zeta_f*ufs*Wf*sign(AlphaF);
end

%Check for the rear peak slip angle
if abs(AlphaR) < AlphaSlipR
    %Calculate the force on the rear tire
    Force_r = -C_ar*tan(AlphaR) + ...
        ((C_ar^2)/(3*zeta_r*ur*Wr))*abs(tan(AlphaR))*tan(AlphaR) - ...
        ((C_ar^3)/(27*(zeta_r^2)*(ur^2)*(Wr^2)))*((tan(AlphaR))^3);
else
    %Calculate the force on the rear tire
    Force_r = -zeta_r*urs*Wr*sign(AlphaR);
end
end


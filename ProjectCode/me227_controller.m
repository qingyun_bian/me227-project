function [ delta_rad, Fx_N ] = me227_controller( s_m, e_m, deltaPsi_rad, Ux_mps, Uy_mps, r_radps, modeSelector, path)
% ME227_CONTROLLER 
%   Alice Li, Amelia Bian, Gabrielle Sebaldt, Nathaniel Agharese
%   
%   The state and controls follow the notation in class and have the units
%   appended.  The parameter path matches what you have in simulation i.e.
%   it has the following fields s_m, k_1pm, psi_rad, posE_m, and posN_m.

    persistent esum;
    persistent e_m_last;
    dt = 0.005;

    g = 9.81;                       %gravitational acceleration (m/s^2)
    m = 1659;                       %kg

    Kdr = 5100;
    Kff = 1500;
    Ux = Ux_mps;
    Ux_des_m = path.Ux_des;
    Uxdot_des_m = path.Uxdot_des;
    
    frr = 0.0157;                   % coefficient of rolling resistance
	Frr = frr * m * g;
    rho = 1.225;
    C_DA = 0.594;
    
    L         = 2.468;
	C_af_linear = 188000;           %Linear front wheel stiffness (N/rad)
	C_ar_linear = 203000;           %Linear front wheel stiffness (N/rad)
  	a = 0.423 * L;                  %Length from front to COM   (m)
	b = 0.577 * L;                  %Length from rear to COM    (m)


    %Limitations of force output to stay within acceleration (m*a_max)
    Fx_max = m*3;
    Fx_min = m*(-4);
    
    %Limitaions of delta output to stay within reasonal tire angles
    delta_max = 0.34;           %radians
    delta_min = -delta_max/2;   %radians

    %PID gain
    Kp = -.5; %5 %-0.5
    Kd = -1.7;%-5;    %-10  5  2
    Ki = -.1*dt; %2 %-0.1
    
    %Lookahead gain
    Kla = 12000;
    xla = 15; 

    esum_max = abs(deg2rad(20)/Ki); 
    esum_min = -esum_max;

    if isempty(esum) || isnan(esum)
        esum = 0;
    end
    if isempty(e_m_last) || isnan(e_m_last)
        e_m_last = 0;
    end

    %perturb error with normally distributed number with variance = 5 cm
%     e_m = e_m*(1+randn(1)*0.02);
    %e_m = e_m+randn(1)*0.03;

    % simulate some form of head wind
%     Ux_mps*(1+randn(1)*0.10)
%     Ux_mps = Ux_mps*(1+randn(1)*0.10);

    %#codegen % this directive is needed to run your code on the car
    if modeSelector == 1 % run your first steering control scheme
        %[delta_rad,Fx_N] = LookaheadController(s_m,e_m,Ux_mps,deltaPsi_rad,path);
        
        % look up K for path in parking lot
        Kappa = interp1(path.s_m, path.k_1pm, s_m);

        %Calculate delta
        K = (m/L)*((b/C_af_linear)-(a/C_ar_linear));
        psi_ss = Kappa*(((m*a*(Ux^2))/(L*C_ar_linear)) - b);
        delta_ff = (Kla*xla/C_af_linear)*psi_ss + Kappa*(L + K*(Ux^2));
        delta_rad = (-Kla*(e_m + xla*deltaPsi_rad)/C_af_linear) + delta_ff;

        %Interpolate to get Ux_des and Uxdot_des wrt time step
        Ux_des = interp1(path.s_m, Ux_des_m, s_m);
        Uxdot_des = interp1(path.s_m, Uxdot_des_m, s_m);

        %Calculate the longitudinal forces on each wheel
        Fd = rho * Ux^2 * C_DA/2;
        Fx = Kdr*(Ux_des-Ux) + Kff*Uxdot_des + Frr + Fd;% + randn(1)*3400;
        
        if Fx > Fx_max
            Fx_N = Fx_max;
        elseif Fx < Fx_min
            Fx_N = Fx_min;
        else
            Fx_N = Fx;
        end
        
    else % run your second steering controller
        esum = esum + e_m;

        % if we exceed 15 deg delta
        if esum > esum_max
            esum = esum_max;
        elseif esum < esum_min
            esum = esum_min;
        end

%         e_dot = (e_m-e_m_last)/dt;
        e_dot = Uy_mps*cos(deltaPsi_rad) + Ux_mps*sin(deltaPsi_rad);
%         [delta_rad,Fx_N] = PIDController(s_m,e_m,esum,e_dot,Ux_mps,path);
        
        %Calculate delta
        delta_rad = Kp*e_m + Kd*e_dot + Ki*esum;
        
        %Make sure delta is reasonable
        if delta_rad > delta_max
            delta_rad = delta_max;
        elseif delta_rad < delta_min
            delta_rad = delta_min;
        end
            

        %Interpolate to get Ux_des and Uxdot_des wrt time step
        Ux_des = interp1(path.s_m, Ux_des_m, s_m);
        Uxdot_des = interp1(path.s_m, Uxdot_des_m, s_m);

        %Calculate the longitudinal forces on each wheel
        Fd = rho * Ux^2 * C_DA/2;
        Fx = Kdr*(Ux_des-Ux) + Kff*Uxdot_des + Frr + Fd;% + randn(1)*3400;
        
        if Fx > Fx_max
            Fx_N = Fx_max;
        elseif Fx < Fx_min
            Fx_N = Fx_min;
        else
            Fx_N = Fx;
        end
        
        e_m_last = e_m;
    end

end

function [Ux_des,Uxdot_des] = ClothoidUx(ConstNL,Input,Increment,path)
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here

    %Make vars easier to read
    Uxdot_des = Input.Uxdot_des;
    Ux_des = Input.Ux_des;
    ds = ConstNL.ds;
    a_max = ConstNL.a_max;

    %Check if we're entering or exiting the clothoid
    if Increment > 0
        Start = ConstNL.clothoid_start;
        End = ConstNL.clothoid_end;
        %Set the correct index for setting the speed at the edge of the
        %curve
        % Ux_init = ConstNL.clothoid_start - 1;
    else
        End = ConstNL.clothoid_start;
        Start = ConstNL.clothoid_end;
        %Set the correct index for setting the speed at the edge of the
        %curve
        % Ux_init = ConstNL.clothoid_end + 1;
    end
    
    %Ux_des(Ux_init) = ConstNL.v_arc_max;
    %Eulerian integration to fill in Ux on the clothoid
    %Note: The K at the same index of the desired speed must be used
    for i=Start:Increment:End
        
        %If entering an acceleration clothoid
        if Input.AccelClothoid == 1
            %Get the expected longitudinal speeds and accels
            Ux_des_new = Ux_des(i-Increment) ...
                + (ds/Ux_des(i-Increment))*sqrt((a_max)^2-(path.k_1pm(i)*Ux_des(i-Increment)^2)^2);
            dt = ds/Ux_des(i-Increment);
            Uxdot_des_new = abs(Ux_des_new - Ux_des(i-Increment))/dt*Increment;
            % If our longitudinal accel is greater than the max
            if Uxdot_des_new > ConstNL.ax_max
                a_max = ConstNL.ax_max;
                Ux_des_new = Ux_des(i-Increment) ...
                    + (ds/Ux_des(i-Increment))*sqrt((a_max)^2-(path.k_1pm(i)*Ux_des(i-Increment)^2)^2);
                dt = ds/Ux_des(i-Increment);
                Uxdot_des_new = abs(Ux_des_new - Ux_des(i-Increment))/dt*Increment;
            end
            Ux_des(i) = Ux_des_new;
            Uxdot_des(i) = Uxdot_des_new;
            
            %Special case for first curve of first lap
        elseif Input.FirstCurve == 1
%             Uxdot_des(i) = -Uxdot_des(i);
            %if we're at the last entry of the clothoid, don't do further
            %calculations
            if i == End
                Ux_des(i) = Ux_des(i-Increment);
                break
            end
            
            %Get the new speed based on the max acceleration
            Ux_des_new = Ux_des(i-Increment) ...
                - (ds/Ux_des(i-Increment))*sqrt((a_max)^2-(path.k_1pm(i)*Ux_des(i-Increment)^2)^2);
            
            %Decelerate if the last speed is above maximum speed of the curve
            if Ux_des(i-1) > ConstNL.v_arc_max
                Ux_des(i) = Ux_des_new;
            else
                Ux_des(i) = Ux_des(i-Increment);
            end
            
            %Get the acceleration
            dt = ds/Ux_des(i-Increment);
            Uxdot_des(i) = (Ux_des(i) - Ux_des(i-Increment))/dt;
            %If entering a deceleration clothoid that isn't the 1st
            %clothoid
        else
            %Get the expected longitudinal speeds and accels
            Ux_des(i) = Ux_des(i-Increment) ...
                + (ds/Ux_des(i-Increment))*sqrt((a_max)^2-(path.k_1pm(i)*Ux_des(i-Increment)^2)^2);
            dt = ds/Ux_des(i-Increment);
            Uxdot_des(i) = abs(Ux_des(i) - Ux_des(i-Increment))/dt*Increment; 
        end
        Input.Ux_des = Ux_des;
    end
end


function [ConstNL, Input, veh] = VehicleParams

%Define Inertia constants
g = 9.81;                       %gravitational acceleration (m/s^2)

%Define the nonlinear model constants for Shelly
ConstNL.C_af      = 275000;               %Front wheel stiffness      (N/rad)
ConstNL.C_ar      = 265000;               %Rear wheel stiffness       (N/rad)
ConstNL.C_af_linear = 188000;             %Linear front wheel stiffness (N/rad)
ConstNL.C_ar_linear = 203000;             %Linear front wheel stiffness (N/rad)
ConstNL.m         = 1659;                    %Mass                       (kg)
ConstNL.Iz        = 2447;                   %Ineria                     (kg/m^2)
ConstNL.L         = 2.468;                    %Wheelbase                  (m)
ConstNL.Wf        = 0.577*ConstNL.m*g;    %Front weight distribution  (%)
ConstNL.Wr        = 0.423*ConstNL.m*g;    %Rear weight distribution   (%)
ConstNL.a         = 0.423*ConstNL.L;      %Length from front to COM   (m)
ConstNL.b         = 0.577*ConstNL.L;      %Length from rear to COM    (m)
ConstNL.Front     = 1;
ConstNL.Rear      = 2;
ConstNL.Kla       = 10000;                 %Look ahead gain            (N/m)

ConstNL.uf      = 0.97;                 %Front wheel friction   coefficient
ConstNL.ufs     = 0.97;                 %Front wheel friction   coefficient
ConstNL.ur      = 1.03;                 %Rear wheel friction    coefficient
ConstNL.urs     = 1.03;                 %Front wheel friction   coefficient

ConstNL.rho = 1.225;
ConstNL.C_DA = 0.594;

% vehicle parameters
veh.L = ConstNL.L;                    % wheelbase
veh.a = ConstNL.a;                  % distance from reference point to front axle
veh.b = ConstNL.b;                      % distance from reference point to rear axle
veh.rW = 0.35;                  % tire radius

% New project parameters here
frr     = 0.0157;                % coefficient of rolling resistance
ConstNL.Frr = frr * ConstNL.m * g;
%Define Kff
ConstNL.Kff = 1500; % was at 700 previously

%Define Kdrive
ax = 0.1*g;                             %m/s^2
Fxtot = ax*ConstNL.m;                           %g
Udiff = 1;                              %m/s
ConstNL.Kdr = Fxtot/Udiff;              %N/m
ConstNL.Kdr = 7000;

%Define the input for the nonlinear simulation
Input.Kappa = 0;
Input.xla   = 15;

%Define initial conditions in the input struct
Input.e     = 0;
Input.Ux    = 0.1;
end
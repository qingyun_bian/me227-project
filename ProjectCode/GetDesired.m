function [Ux_des, Uxdot_des] = GetDesired(path)
clc; close all;


Input.Ux_des = zeros(size(path.s_m));
Input.Uxdot_des = zeros(size(path.s_m));

C_Start = 1;
C_End = 2;
%Note: These reflect indexes of significant (more than 0.345%). This is
%necessary to get real values for the speed at the 3rd and 4th clothoids.
Clothoid = [50 159 302 411 553 663 805 914; 96 205 347 456 599 708 851 960]; % indices
EndFirstStraight = Clothoid(C_Start,1)-1; % index

ConstNL.ds = 0.25; % m

ConstNL.ay_max = 4;         % m/s^2
ConstNL.a_max = 4;          % m/s^2
ConstNL.ax_max = 3;         % m/s^2
ConstNL.ax_min = -4;        % m/s^2
ConstNL.ax_stop = -1;       % m/s^2
ConstNL.arc_radius = 8.7;   % m

%Maximum speed attainable in constant radius curves with ay_max
ConstNL.v_arc_max = sqrt(ConstNL.ay_max*ConstNL.arc_radius);

%Start straightaway
%Accelerate as much as possible in first straight
Input.Uxdot_des(1:EndFirstStraight) = ConstNL.ax_max;

%Calculate resulting speed in first straight
for n=1:(EndFirstStraight-1)
    Input.Ux_des(n+1) = sqrt(Input.Ux_des(n)^2+2*abs(Input.Uxdot_des(n))*ConstNL.ds);
end

%Calculate speeds and accels for curves
for j=1:length(Clothoid)
    ConstNL.clothoid_start = Clothoid(C_Start,j);
    ConstNL.clothoid_end = Clothoid(C_End,j);
    % Get speed/acceleration at constant curvatures
    if (j==1 || j==3 || j==5 || j==7)
        Input.AccelClothoid = 0;
        %Switch the direction for iterating through the first clothoid
        if (j==1)
            Input.FirstCurve = 1;   % First curve flag is on
            Increment = 1;
            %Get speed/acceleration on clothoids
            [Input.Ux_des,Input.Uxdot_des] = ClothoidUx(ConstNL,Input,Increment,path);
            % Start and end of constant curvature
            Start = Clothoid(C_End,j)+1;
            End = Clothoid(C_Start,j+1);
            % Set speed in constant radius curve to max allowable
            Input.Ux_des(Start:End) = Input.Ux_des(Start-1);
            Input.FirstCurve = 0; % set to 0 to indicate it is no longer the first curve
        else
            Increment = -1;
            % Start and end of constant curvature
            Start = Clothoid(C_End,j)+1;
            End = Clothoid(C_Start,j+1);
            % Set speed in constant radius curve to max allowable
            Input.Ux_des(Start:End) = ConstNL.v_arc_max;
            %Get speed/acceleration on clothoids
            [Input.Ux_des,Input.Uxdot_des] = ClothoidUx(ConstNL,Input,Increment,path);
        end
    else
        Input.AccelClothoid = 1;
        Increment = 1;
        %Get speed/acceleration on clothoids
        [Input.Ux_des,Input.Uxdot_des] = ClothoidUx(ConstNL,Input,Increment,path);
    end
end

%Calculate speeds and accelerations for the straight aways
syms t1 t2 Ux1 Ux2 L
func1 = (Ux1+ConstNL.ax_max*t1+Ux1)*t1/2 + (Ux2+Ux1+ConstNL.ax_max*t1)*(t2-t1)/2 == L;
func2 = Ux1+ConstNL.ax_max*t1+ConstNL.ax_min*(t2-t1)== Ux2;
S = vpasolve([func1, func2], [t1,t2]);

Straight = [206 457 709; 301 552 804];

%Iterate through the full straights
for k=1:length(Straight)
    Start = Straight(C_Start,k);
    End = Straight(C_End,k);
    Ux_peak = double(StraightPeak(Input.Ux_des(205),Input.Ux_des(302),S,ConstNL,(ConstNL.ds*(302-205))));
    [Input.Ux_des,Input.Uxdot_des] = StraightUx(Input,ConstNL,Ux_peak,Start,End);
end

%Final straightaway
LastIndex = 1009;
%Deccelerate as much as possible in the final straight
Input.Uxdot_des(Straight(C_End,length(Straight)):EndFirstStraight) = ConstNL.ax_max;

%Calculate the speed profile of the last straight away until the car stops
for m=Clothoid(C_End,length(Clothoid)):LastIndex
    Ux_des_new = sqrt(Input.Ux_des(m-1)^2+2*ConstNL.ax_min*ConstNL.ds);
    if(Ux_des_new > 0)
        Input.Ux_des(m) = Ux_des_new;
        Input.Uxdot_des(m) = ConstNL.ax_min;
    else
        Input.Uxdot_des(m:LastIndex) = ConstNL.ax_stop;
        break;
    end
end
Ux_des = Input.Ux_des;
Uxdot_des = Input.Uxdot_des;
end
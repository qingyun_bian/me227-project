function [Ux_des,Uxdot_des] = StraightUx(Input,ConstNL,Ux_peak,Start,End)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
Ux_des = Input.Ux_des;
Uxdot_des = Input.Uxdot_des;
Accel = 1;
%iterate over the straight away
for i=Start:End
    Ux_des_new = sqrt(Ux_des(i-1)^2+2*ConstNL.ax_max*ConstNL.ds);
    %If you will be past your max Ux, start to deccelerate
    if  Accel == 0
        Ux_des_new = sqrt(Ux_des(i-1)^2+2*ConstNL.ax_min*ConstNL.ds);
        Ux_des(i) = Ux_des_new;
        Uxdot_des(i) = ConstNL.ax_min;
    else
        %Mark that you're no longer accelerating
        if (Ux_des_new > Ux_peak)
            Accel = 0;
            Ux_des_new = sqrt(Ux_des(i-1)^2+2*ConstNL.ax_min*ConstNL.ds);
        end
        Ux_des(i) = Ux_des_new;
        Uxdot_des(i) = ConstNL.ax_max;
    end
end
end


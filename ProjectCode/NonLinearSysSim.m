function [r,Ux,Uy,psi,s,e,delta] = NonLinearSysSim(Const,Input,Time,path_track)
%NonLinearTireSim Summary of this function goes here
%   Detailed explanation goes here
% allocate space for simulation data and set initial conditions
Alpha       = zeros(Time.N,2);
Forces_y    = zeros(Time.N,2);
Forces_x    = zeros(Time.N,2);

Ux          = zeros(Time.N,1);
Uy          = zeros(Time.N,1);
r           = zeros(Time.N,1);
s           = zeros(Time.N,1);
e           = zeros(Time.N,1);
psi         = zeros(Time.N,1);

Ux_dot      = zeros(Time.N,1);
Uy_dot      = zeros(Time.N,1);
r_dot       = zeros(Time.N,1);
s_dot       = zeros(Time.N,1);
e_dot       = zeros(Time.N,1);
psi_dot     = zeros(Time.N,1);

s_m         = zeros(Time.N,1);
delta       = zeros(Time.N,1);

Input.Alpha = Alpha;

%Create variables from the struct
Kla = Const.Kla;
xla = Input.xla;
C_af_linear = Const.C_af_linear;
C_ar_linear = Const.C_ar_linear;
Front = Const.Front;
Rear = Const.Rear;
a = Const.a;
b = Const.b;
Kdr = Const.Kdr;
Kff = Const.Kff;
Uxdes = Input.Ux_des;
Uxdes_dot = Input.Ux_des_dot;
m = Const.m;
Iz = Const.Iz;
L = Const.L;

%Calculate the peak slip anlges
Input.AlphaSlipF = atan(3*Const.uf*Const.Wf/Const.C_af);
Input.AlphaSlipR = atan(3*Const.ur*Const.Wr/Const.C_ar);

%Apply initial conditions
e(1)    = Input.e;
Ux(1)   = Input.Ux;
delta(1)   = 0;

for idx = 1:Time.N
    % look up K for undulating path (2b only)
    Kappa = interp1(path_track.s_m, path_track.k_1pm, s_m(idx));
    
    %     Calculate delta
    K = (m/L)*((b/C_af_linear)-(a/C_ar_linear));
    psi_ss = Kappa*(((m*a*(Ux(idx)^2))/(L*C_ar_linear)) - b);
    delta_ff = (Kla*xla/C_af_linear)*psi_ss + ...
        Kappa*(L + K*(Ux(idx)^2));
    delta(idx) = (-Kla*(e(idx) + xla*psi(idx))/C_af_linear) + delta_ff;
    
    %Calculate the slip angles
    [Alpha(idx,Front), Alpha(idx,Rear)] = SlipAngle(Uy(idx),Ux(idx),delta(idx),r(idx),a,b);
    Input.AlphaF = Alpha(idx,Front);
    Input.AlphaR = Alpha(idx,Rear);
    
    %Calculate the lateral forces from slip angles
    [Forces_y(idx,Front), Forces_y(idx,Rear)] = SlipForceNL(Const,Input);
    if Ux == 0
        Forces_y(idx, Front) = 0;
        Forces_y(idx, Rear) = 0;
    end
    
    %Calculate the longitudinal forces on each wheel
    Fd = Const.rho * Ux(idx)^2 * Const.C_DA/2;
    Fx_total = Kdr*(Uxdes(idx)-Ux(idx)) + Kff*Uxdes_dot(idx) + Const.Frr...
        + Fd;
    Forces_x(idx,:) = [Fx_total * 0.6, Fx_total * 0.4];
    
    %Calculate the derivatives of the state variables
    Ux_dot(idx) = (Forces_x(idx,Front)/m)*cos(delta(idx)) + ...
        (Forces_x(idx,Rear)/m) - (Forces_y(idx,Front)/m)*sin(delta(idx)) + ...
        r(idx)*Uy(idx);
    Uy_dot(idx) = (Forces_y(idx,Front)/m)*cos(delta(idx)) + ...
        (Forces_y(idx,Rear)/m) + (Forces_x(idx,Front)/m)*sin(delta(idx)) - ...
        r(idx)*Ux(idx);
    r_dot(idx)  = (a*Forces_y(idx,Front)*cos(delta(idx)) +...
        a*Forces_x(idx,Front)*sin(delta(idx)) - ...
        b*Forces_y(idx,Rear))/Iz;
    s_dot(idx) = (1/(1-e(idx)*Kappa))*(Ux(idx)*cos(psi(idx)) -...
        Uy(idx)*sin(psi(idx)));
    e_dot(idx) = Uy(idx)*cos(psi(idx)) + Ux(idx)*sin(psi(idx));
    psi_dot(idx) = r(idx) - Kappa*s_dot(idx);
    
    % only update next state if we are not at end of simulation
    if idx < Time.N
        % euler integration
        Ux(idx+1)   = Ux(idx) + Ux_dot(idx)*Time.dT;
        Uy(idx+1)   = Uy(idx) + Uy_dot(idx)*Time.dT;
        r(idx+1)    = r(idx) + r_dot(idx)*Time.dT;
        s(idx+1)    = s(idx) + s_dot(idx)*Time.dT;
        s_m(idx+1)  = s(idx+1);
        e(idx+1)    = e(idx) + e_dot(idx)*Time.dT;
        psi(idx+1)    = psi(idx) + psi_dot(idx)*Time.dT;
    end
end

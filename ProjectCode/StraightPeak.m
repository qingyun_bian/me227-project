function [Ux_peak] = StraightPeak(Ux_Start,Ux_End,Solution,ConstNL,Distance)
%StraightPeak Calculates the peak possible speed at the straight away
%   Detailed explanation goes here
Ux1 = Ux_Start;
Ux2 = Ux_End;
L = Distance;

time1 = subs(Solution.t1(1));

Ux_peak = abs(Ux1 + time1*ConstNL.ax_max);
end


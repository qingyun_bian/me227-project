 function [delta_rad,Fx_N] = PIDController(s_m,e_m,e_dot,esum,Ux,path)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    g = 9.81;                       %gravitational acceleration (m/s^2)
    m = 1659;                       %kg

    Kdr = 5100;
    Kff = 1500;
    Ux_des_m = path.Ux_des;
    Uxdot_des_m = path.Uxdot_des;
    
    frr = 0.0157;                   % coefficient of rolling resistance
	Frr = frr * m * g;
    rho = 1.225;
    C_DA = 0.594;
    
    %Limitations of force output to stay within acceleration (m*a_max)
    Fx_max = m*3;
    Fx_min = m*(-4);

    %Lookahead gain
    Kp = 2; %5
    Kd = 0;
    Ki = 0.05; %2
    
    %Calculate delta
    delta_rad = -Kp*e_m + Kd*e_dot - Ki*esum;

    %Interpolate to get Ux_des and Uxdot_des wrt time step
    Ux_des = interp1(path.s_m, Ux_des_m, s_m);
    Uxdot_des = interp1(path.s_m, Uxdot_des_m, s_m);
    
    %Calculate the longitudinal forces on each wheel
    Fd = rho * Ux^2 * C_DA/2;
    Fx = Kdr*(Ux_des-Ux) + Kff*Uxdot_des + Frr + Fd ;%+ randn(1)*3400;
    if Fx > Fx_max
        Fx_N = Fx_max;
    elseif Fx < Fx_min
        Fx_N = Fx_min;
    else
        Fx_N = Fx;
    end
end
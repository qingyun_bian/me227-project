function [e,psi] = LinearErrorSim(Const,Input,Time)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

% allocate space for simulation data and set initial conditions
Alpha       = zeros(Time.N,2);
Forces      = zeros(Time.N,2);
e          = zeros(Time.N,1);
psi           = zeros(Time.N,1);
e_dot      = zeros(Time.N,1);
psi_dot       = zeros(Time.N,1);
e

%Check for instances when a constant is passed in instead of a vector
if length(Input.delta) < 2
    Input.delta = Input.delta*ones([1,Time.N]);
end

if length(Input.Ux) < 2
    Input.Ux = Input.Ux*ones([1,Time.N]);
end

%iterat through the data
for idx = 1:Time.N
    %Avoid divide by zero error
    if Input.Ux(idx) > 1
        %Calculate the slip angles
        [Alpha(idx,Const.Front), Alpha(idx,Const.Rear)] = SlipAngle(Uy(idx),Input.Ux(idx),Input.delta(idx),r(idx),Const.a,Const.b);

        %Calculate the forces from slip angles
        [Forces(idx,Const.Front), Forces(idx,Const.Rear)] = SlipForces(Const.C_af,Const.C_ar,Alpha(idx,Const.Front),Alpha(idx,Const.Rear));

        %Calculate the derivatives of the state variables
        Uy_dot(idx) = (Forces(idx,Const.Front)/Const.m) + (Forces(idx,Const.Rear)/Const.m) - r(idx).*Input.Ux(idx);
        r_dot(idx)  = (Const.a*Forces(idx,Const.Front) - Const.b*Forces(idx,Const.Rear))/Const.Iz;

        % only update next state if we are not at end of simulation
        if idx < Time.N
            % euler integration
            Uy(idx+1)   = Uy(idx) + Uy_dot(idx)*Time.dT;
            r(idx+1)    = r(idx) + r_dot(idx)*Time.dT;
        end
    else
        r(idx) = 0;
        Uy(idx) = 0;
    end
end
end


function [r,Ux,Uy,psi,s,e,delta] = NLSim(Const,Input,Time,path,mode)
%NonLinearTireSim Summary of this function goes here
%   Detailed explanation goes here
% allocate space for simulation data and set initial conditions
    Alpha       = zeros(Time.N,2);
    Forces_y    = zeros(Time.N,2);
    Forces_x    = zeros(Time.N,2);

    Ux          = zeros(Time.N,1);
    Uy          = zeros(Time.N,1);
    r           = zeros(Time.N,1);
    s           = zeros(Time.N,1);
    e           = zeros(Time.N,1);
    psi         = zeros(Time.N,1);

    Ux_dot      = zeros(Time.N,1);
    Uy_dot      = zeros(Time.N,1);
    r_dot       = zeros(Time.N,1);
    s_dot       = zeros(Time.N,1);
    e_dot       = zeros(Time.N,1);
    psi_dot     = zeros(Time.N,1);

    s_m         = zeros(Time.N,1);
    delta       = zeros(Time.N,1);
    
    Fx_plot     = zeros(Time.N,1);

    Input.Alpha = Alpha;

    %Create variables from the struct
    Front = Const.Front;
    Rear = Const.Rear;
    a = Const.a;
    b = Const.b;
    m = Const.m;
    Iz = Const.Iz;

    %Calculate the peak slip anlges
    Input.AlphaSlipF = atan(3*Const.uf*Const.Wf/Const.C_af);
    Input.AlphaSlipR = atan(3*Const.ur*Const.Wr/Const.C_ar);

    %Apply initial conditions
    e(1)    = 0; %Input.e;
    delta(1)   = 0;
    Ux(1)   = 0.1;

    for i = 1:Time.N
        % look up K for undulating path (2b only)
        Kappa = interp1(path.s_m, path.k_1pm, s_m(i));

        %Calculate delta  
        [delta(i),Fx_total] = me227_controller(s_m(i),e(i),psi(i),Ux(i),Uy(i),r(i),mode,path);

        Fx_plot(i) = Fx_total;
        
        %Calculate the slip angles
        [Alpha(i,Front), Alpha(i,Rear)] = SlipAngle(Uy(i),Ux(i),delta(i),r(i),a,b);
        Input.AlphaF = Alpha(i,Front);
        Input.AlphaR = Alpha(i,Rear);
        
        %Calculate the longitudinal forces on each wheel
        Forces_x(i,:) = [Fx_total * 0.6, Fx_total * 0.4];

        %Calculate the lateral forces from slip angles
        [Forces_y(i,Front), Forces_y(i,Rear)] = SlipForcesNLCoupled(Const,Input,Forces_x(i,:));
%         [Forces_y(i,Front), Forces_y(i,Rear)] = SlipForceNL(Const,Input);
        if Ux == 0
            Forces_y(i, Front) = 0;
            Forces_y(i, Rear) = 0;
        end

        %Calculate the derivatives of the state variables
        Ux_dot(i) = (Forces_x(i,Front)/m)*cos(delta(i)) + ...
            (Forces_x(i,Rear)/m) - (Forces_y(i,Front)/m)*sin(delta(i)) + ...
            r(i)*Uy(i);
        Uy_dot(i) = (Forces_y(i,Front)/m)*cos(delta(i)) + ...
            (Forces_y(i,Rear)/m) + (Forces_x(i,Front)/m)*sin(delta(i)) - ...
            r(i)*Ux(i);
        r_dot(i)  = (a*Forces_y(i,Front)*cos(delta(i)) +...
            a*Forces_x(i,Front)*sin(delta(i)) - ...
            b*Forces_y(i,Rear))/Iz;
        s_dot(i) = (1/(1-e(i)*Kappa))*(Ux(i)*cos(psi(i)) -...
            Uy(i)*sin(psi(i)));
        e_dot(i) = Uy(i)*cos(psi(i)) + Ux(i)*sin(psi(i));
        psi_dot(i) = r(i) - Kappa*s_dot(i);

        % only update next state if we are not at end of simulation
        if i < Time.N
            % euler integration
            Ux(i+1)   = Ux(i) + Ux_dot(i)*Time.dT;
            Uy(i+1)   = Uy(i) + Uy_dot(i)*Time.dT;
            r(i+1)    = r(i) + r_dot(i)*Time.dT;
            s(i+1)    = s(i) + s_dot(i)*Time.dT;
            s_m(i+1)  = s(i+1);
            e(i+1)    = e(i) + e_dot(i)*Time.dT;
            psi(i+1)    = psi(i) + psi_dot(i)*Time.dT;
        end
    end
    
    figure(7)
    plot(Time.t_s,Fx_plot)
    title('Fx input')
    hold on
    
    figure(8)
    plot(Time.t_s,delta)
    title('Delta input')
    hold on
end